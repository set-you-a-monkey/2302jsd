package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//集合排序---String
public class SortString {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        list.add("wangkj");
        list.add("传奇老师");
        list.add("国兵afgafsdia");
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
        //自定义排序规则
        //前面-后面=升序
        //后面-前面=降序
        Collections.sort(list, (o1,  o2)-> o1.length()-o2.length());
        System.out.println("list排序后的数据："+list);








        /*
        List<String> list=new ArrayList<>();
        list.add("jack");
        list.add("rose");
        list.add("tom");
        list.add("jerry");
        list.add("black");
        list.add("Kobe");
        System.out.println("lise原始数据："+list);
        Collections.sort(list);
        System.out.println("list排序后的数据："+list);

         */
    }
}
