package collection;

import java.util.*;

public class ListDemo1 {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        list.add("王克晶");
        list.add("传奇asddasf");
        list.add("国斌老师");
        System.out.println("list原始数据："+list);
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length()-o2.length();
            }
        });
        Collections.sort(list,(o1,o2)->o1.length()-o2.length());
        System.out.println("list排序后的顺序："+list);
        Random rand=new Random();
        List<Integer> list1=new ArrayList<>();
        for (int i=0;i<10;i++){
            list1.add(rand.nextInt(100));
        }
        System.out.println("list1原始数据："+list1);
        Collections.sort(list1);
        System.out.println("list1排序后的顺序："+list1);
        Collections.reverse(list1);
        System.out.println("list1反转后的数据："+list1);
        System.out.println("第一个元素为："+list1.get(0));
        List<Point> list2=new ArrayList<>();
        list2.add(new Point(5,8));
        list2.add(new Point(15,60));
        list2.add(new Point(57,89));
        list2.add(new Point(1,4));
        list2.add(new Point(10,8));
        list2.add(new Point(22,35));
        System.out.println("list2原始数据："+list2);
        list2.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.getX()-o2.getX();
            }
        });
        System.out.println("list2排序后的顺序："+list2);
        list2.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                int len1= o1.getX()*o1.getX()-o1.getY()*o1.getY();
                int len2=o2.getX()*o2.getX()-o2.getY()*o2.getY();
                return len1-len2;
            }
        });
        Collections.sort(list2,(o1,o2)->{
            int len1= o1.getX()*o1.getX()-o1.getY()*o1.getY();
            int len2=o2.getX()*o2.getX()-o2.getY()*o2.getY();
            return len1-len2;
        });
        System.out.println("list2排序后的数据："+list2);
        /*
        List<String> list=new ArrayList<>();
        list.add("王克晶");
        list.add("传奇asddasf");
        list.add("国斌老师");
        System.out.println("list原始数据："+list);
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length()-o2.length();
            }
        });
        Collections.sort(list,(o1,o2)->o1.length()-o2.length());
        System.out.println("list排序后的顺序："+list);
        Random rand=new Random();
        List<Integer> list1=new ArrayList<>();
        for (int i=0;i<10;i++){
            list1.add(rand.nextInt(100));
        }
        System.out.println("list1原始数据："+list1);
        Collections.sort(list1);
        System.out.println("list1排序后的顺序："+list1);
        Collections.reverse(list1);
        System.out.println("list1反转后的数据："+list1);
        System.out.println("第一个元素为："+list1.get(0));
        List<Point> list2=new ArrayList<>();
        list2.add(new Point(5,8));
        list2.add(new Point(15,60));
        list2.add(new Point(57,89));
        list2.add(new Point(1,4));
        list2.add(new Point(10,8));
        list2.add(new Point(22,35));
        System.out.println("list2原始数据："+list2);
        list2.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.getX()-o2.getX();
            }
        });
        System.out.println("list2排序后的顺序："+list2);
        list2.sort(new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
               int len1= o1.getX()*o1.getX()-o1.getY()*o1.getY();
               int len2=o2.getX()*o2.getX()-o2.getY()*o2.getY();
               return len1-len2;
            }
        });
        Collections.sort(list2,(o1,o2)->{
            int len1= o1.getX()*o1.getX()-o1.getY()*o1.getY();
            int len2=o2.getX()*o2.getX()-o2.getY()*o2.getY();
            return len1-len2;
        });
        System.out.println("list2排序后的数据："+list2);

         */










        /*
        List<Integer> list=new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add(i*10);
        }
        System.out.println("list:"+list);
        List<Integer> subList=list.subList(3,8);
        System.out.println(subList);

         */




        /*
        List<Integer> list=new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add(i*10);
        }
        System.out.println("list:"+list);
        List<Integer> subList=list.subList(3,8);
        System.out.println("subList:"+subList);

         */
























        /*
        List<String> list=new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("one");
        System.out.println(list);
        String e=list.get(2);
        System.out.println(e);
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        Iterator<String> it=list.listIterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        for (String s:list){
            System.out.println(s);
        }
       list.set(2,"six");
        String old=list.set(2,"six");
        System.out.println("list:"+list);
        System.out.println(old);
        list.remove(2);
        String s=list.remove(2);
        System.out.println("list:"+list);
        System.out.println(s);
        list.add(3,"seven");
        System.out.println(list);

         */

























        /*
        List<String> list=new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("one");
        System.out.println("list:"+list);
        String e=list.get(2);//获取指定下标所对应的元素
        System.out.println(e);
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
            list.set(2,"six");
            String old= list.set(2,"six");
            System.out.println(list);
            System.out.println(old);
            list.remove(2);
            String s=list.remove(2);
        }
        list.add(3,"seven");
        System.out.println(list);

         */



    }
}
