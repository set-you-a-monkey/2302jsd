package collection;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionDemo1 {
    public CollectionDemo1() {
    }

    public static void main(String[] args) {
     Collection<String> c=new ArrayList<>();
     c.add("a");
        c.add("b");
        c.add("c");
        c.add("d");
        c.add("f");
        System.out.println(c);
        System.out.println("size:"+ c.size());
        System.out.println("是否为空集："+c.isEmpty());
        c.clear();System.out.println("合集已经清空");
        System.out.println(c);
        System.out.println("是否为空集："+c.isEmpty());
        Collection cc=new ArrayList();
        cc.add(new Point1(1,2));
        cc.add(new Point1(3,4));
        cc.add(new Point1(5,6));
        cc.add(new Point1(7,8));
        cc.add(new Point1(9,0));
        Point1 p=new Point1(1,2);
        boolean contains=c.contains(p);
        System.out.println("是否包含："+contains);
        c.remove(p);
        System.out.println(c);
        Collection<String> c1=new ArrayList<>();
        c1.add("java");
        c1.add("c++");
        c1.add(".net");
        System.out.println(c1);
        Collection<String> c2=new ArrayList<>();
        c2.add("android");
        c2.add("ios");
        c2.add("java");
        System.out.println(c2);
        c1.addAll(c2);
        System.out.println("c1:"+c2);
        System.out.println("c2:"+c2);
        Collection<String> c3=new ArrayList<>();
        c3.add("c++");
        c3.add("android");
        c3.add("java");
        System.out.println("c3:"+c3);
        boolean contains1=c1.contains(c3);
        System.out.println("是否包含："+contains1);
        c1.retainAll(c3);
        System.out.println("c1:"+c1);
        System.out.println("c3:"+c3);
        c1.remove(c3);
        System.out.println("c1:"+c1);
        System.out.println("c3:"+c3);










    }
}
   /*


        Collection<String> c=new ArrayList<>();
        c.add("a");
        c.add("b");
        c.add("c");
        c.add("d");
        c.add("f");
        System.out.println(c);
        System.out.println("size:"+c.size());
        System.out.println("是否为空集："+c.isEmpty());
        c.clear();
        System.out.println("集合已清空");
        System.out.println(c);
        System.out.println("是否已清空："+c.isEmpty());
        Collection c1=new ArrayList();
        c1.add(new Point1(1,2));
        c1.add(new Point1(3,4));
        c1.add(new Point1(5,6));
        c1.add(new Point1(7,8));
        c1.add(new Point1(9,0));
        System.out.println(c);
        Point1 P=new Point1(1,2);
        boolean contains=c.contains(P);
        System.out.println("是否包含："+contains);
        c.remove(P);
        System.out.println(c1);
        Collection<String> c2=new ArrayList<>();
        c2.add("java");
        c2.add("c++");
        c2.add(".net");
        System.out.println("c2:"+c2);
        Collection<String> c3=new ArrayList<>();
        c3.add("android");
        c3.add("ios");
        c3.add("java");
        System.out.println("c3:"+c3);
        c2.addAll(c3);
        System.out.println("c2:"+c2);
        System.out.println("c3:"+c3);
        Collection<String> c4=new ArrayList<>();
        c4.add("c++");
        c4.add("android");
        c4.add("php");
        System.out.println("c4:"+c4);
        c2.retainAll(c4);
        System.out.println("c2:"+c2);
        System.out.println("c4:"+c4);
        c2.removeAll(c4);
        System.out.println("c2:"+c2);
        System.out.println("c4:"+c4);

         */
