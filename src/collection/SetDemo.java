package collection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//Set集合的演示
public class SetDemo {
    public static void main(String[] args) {
        //小面试题：如何去重？
        List<String> list=new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        list.add("one");
        System.out.println(list);
        Set<String> set=new HashSet<>();
        set.addAll(list);
        System.out.println(set);












        /*
        Set<String> set=new HashSet<>();
        set.add("one");
        set.add("two");
        set.add("three");
        set.add("four");
        set.add("five");
        set.add("one");//无法被正确的被添加进去，因为set集是不可被重复集合，并且大部分无序
        System.out.println(set);

         */


    }
}
