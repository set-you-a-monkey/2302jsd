package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SortInteger {
    public static void main(String[] args) {
        Random random=new Random();
        List<Integer> list=new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add(random.nextInt(100) );
        }
        System.out.println("list原始数据:"+list);
        Collections.sort(list);
        System.out.println("排序后的list:"+list);//有序，由小到大
        Collections.reverse(list);
        System.out.println("反转后的list:"+list);//由大到小
    }
}
