package collection;

import java.util.*;

public class ListDemo {
    public static void main(String[] args) {
        List<String> list=new LinkedList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        list.add("five");
        System.out.println("list；"+list);
        String e=list.get(2);
        System.out.println(e);
        Iterator<String> it= list.listIterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
        for (String s:list){
            System.out.println(s);
        }
        for (int i=0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println("------------------------------");
        String old=list.set(2,"six");
        System.out.println("list:"+list);
        System.out.println(old);
        //删除
        String s=list.remove(2);//删除下标为2的元素，并将被删除的元素返回给s
        System.out.println(list);
        System.out.println(s);
        //重载add的方法
        list.add(3,"seven");//在list下标为3的位置插入seven
        System.out.println(list);




    }
}
