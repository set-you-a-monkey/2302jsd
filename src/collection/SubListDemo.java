package collection;

import java.util.ArrayList;
import java.util.List;

public class SubListDemo {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        for (int i=0;i<10;i++){
            list.add(i*10);
        }
        System.out.println(list);
        List<Integer> sublist=list.subList(3,8);
        System.out.println(sublist);
        //将每个子集扩大十倍
        for (int i=0;i<sublist.size();i++){
            sublist.set(i,sublist.get(i)*10);
        }
        System.out.println("sublist:"+sublist);
        //注意：对子集的操作就是对原集合对应的元素操作
        System.out.println("list:"+list);

        list.set(3,1000);//将原集合的下标为3的元素改为1000
        System.out.println("list:"+list);
        //元集合数据改变后，自己数据也跟着改变了
        System.out.println("sublist:"+sublist);
        list.remove(0);
        System.out.println("list:"+list);
        System.out.println("sublist:"+sublist);
    }
}
