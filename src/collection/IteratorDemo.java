package collection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorDemo {
    public static void main(String[] args) {
        Collection c=new ArrayList();
        c.add("one");
        c.add("#");
        c.add("two");
        c.add("#");
        c.add("three");
        c.add("#");
        c.add("four");
        c.add("#");
        c.add("five");
        c.add("#");
        System.out.println(c);
        //迭代器的常用方法
        //boolean hasnext()-----问
        //询问集合是否还有下一个元素可以迭代
        //注意：迭代器默认开始的位置在集合前第一个元素之前
        //2）
        Iterator it=c.iterator();
        while(it.hasNext()){
            String str=(String)it.next();
            System.out.println(str);
            if ("#".equals(str)){
                //c.remove(str);//迭代器遍历过程中不允许通过集合的方式来增删元素，否则会爆异常
                it.remove();
            }
        }
        System.out.println(c);

    }
}
