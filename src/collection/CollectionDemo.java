package collection;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionDemo {
    public static void main(String[] args) {
      Collection c=new ArrayList();
      c.add(new Point(1,2));
      c.add(new Point(3,4));
      c.add(new Point(5,6));
      c.add(new Point(7,8));
      c.add(new Point(9,0));
      c.add(new Point(1,2));
      //[元素1.toString(),元素2.toString(),元素3.toString,....]
      System.out.println(c);
        //boolean contains(object o)
        //判定当前集合元素是否包含给定元素
        //判断依据给定元素是否与当前集合存在equals（）比较为TRUE的情况
        Point p=new Point(1,2);
        boolean contains=c.contains(p);
        System.out.println( "是否包含："+ contains);
        //boolean remove(object o):-------一般不接受boolean结果
        //从当前集合中删除与给定元素o的equals（）比较为true的元素
        //若重复存在只删除一次
        c.remove(p);
        System.out.println(c);
        //集合存放的是元素的引用（地址）
        Collection cc=new ArrayList();
        Point pp=new Point(1,2);
        cc.add(pp);
        System.out.println("pp:"+pp);
        System.out.println("cc:"+cc);
        pp.setX(100);
        System.out.println("cc:"+cc);
        System.out.println("pp:"+pp);
    }
}
  /*
        Collection c=new ArrayList();
        c.add("one");
        c.add("two");
        c.add("three");
        c.add("four");
        c.add("five");
        System.out.println(c);
        System.out.println("size:"+c.size());//输出集合的元素
        //isEmpty()
        System.out.println("是否为空集："+c.isEmpty());//false
        c.clear();//清空集合
        System.out.println("集合已清空");
        System.out.println(c);
        System.out.println("size:"+c.size());
        System.out.println("是否为空集："+c.isEmpty());

         */