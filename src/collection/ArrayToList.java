package collection;

import java.util.Arrays;
import java.util.List;

//数组的抓换为集合
public class ArrayToList {
    public static void main(String[] args) {
        String[] array = {"one", "two", "three", "four", "five"};
        System.out.println("array:" + Arrays.toString(array));
        List<String> list = Arrays.asList(array);
        System.out.println("List:" + list);
        //对数组操作后，集合也会做出相应的改变
        array[1] = "six";
        System.out.println("array:"+Arrays.toString(array));
        System.out.println("List:"+list);
        //对集合操作后，数组也会做出相应的改变
        list.set(2,"seven");//将第三个元素改为seven
        System.out.println(Arrays.toString(array));
        System.out.println("List:"+list);


        list.add("!!!!");//运行时会发生操作异常




    }
}
