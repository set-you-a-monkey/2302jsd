package collection;

import java.util.ArrayList;
import java.util.Collection;

//集合间的操作
public class CollectionOperDemo {
    public static void main(String[] args) {
        Collection c1=new ArrayList();
        c1.add("java");
        c1.add("c++");
        c1.add(".net");
        System.out.println( "c1:"+ c1);
        Collection c2=new ArrayList();
        c2.add("android");
        c2.add("ios");
        c2.add("java");
        System.out.println(c2);
        c1.addAll(c2);
        System.out.println(c1);
        System.out.println(c2);
        Collection c3=new ArrayList();
        c3.add("c++");
        c3.add("java");
        c3.add("php");
        System.out.println("c3:"+c3);
        boolean contains=c1.containsAll(c3);
        System.out.println(  "是否包含："+ contains);
        /*
        //取交集：c1中仅保留c3所共有的元素，而c3不变
        c1.retainAll(c3);
        System.out.println("c1:"+c1);
        System.out.println("c3:"+c3);

         */
        //删交集：将c1与c3共有得元素删掉，c3不变
        c1.removeAll(c3);
        System.out.println("c1:"+c1);
        System.out.println("c3:"+c3);
    }
}
