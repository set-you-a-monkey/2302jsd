package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LambdaDemo {
    public static void main(String[] args) {
        List<String> list=new ArrayList<>();
        //匿名内部类写法
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.length()-o2.length();
            }
        });
        //Lambda表达式写法
        Collections.sort(list, (String o1, String o2)-> {
                return o1.length()-o2.length();
            }
        );
        //Lambda表达式中的参数类型可以不写
        Collections.sort(list, ( o1,  o2)-> {
                    return o1.length()-o2.length();
                }
        );
        //Lambda表达式方法题中只有一句代码，方法体中的{}可以不写，如果这句话中有return，也一并不写
        Collections.sort(list, ( o1,  o2)->  o1.length()-o2.length());

    }
}
