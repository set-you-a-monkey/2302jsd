package socket;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

//聊天室的客户端
public class Client {
    private Socket socket;
    public Client(){
        try {
            System.out.println("正在与服务器连接。。。");
            socket=new Socket("localhost",8088);
            System.out.println("与服务器连接成功!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void start(){
        try {
            OutputStream out=socket.getOutputStream();
            OutputStreamWriter osw=new OutputStreamWriter(out, StandardCharsets.UTF_8);
            BufferedWriter bw=new BufferedWriter(osw);
            PrintWriter pw=new PrintWriter(bw,true);
            Scanner scan=new Scanner(System.in);
            while (true){
                String line=scan.nextLine();
                if ("exit".equals(line)){
                    break;
                }

              pw.println(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {
        Client client=new Client();
        client.start();
    }
}
