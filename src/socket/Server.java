package socket;
//聊天室的服务端
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;


public class Server {
    private ServerSocket serverSocket;
    public Server(){
        try {
            System.out.println("正在启动服务器");
            serverSocket=new ServerSocket(8088);
            System.out.println("服务器启动完毕!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void Start(){
        try {
            while (true){
                System.out.println("等待客户端连接");
                Socket socket=serverSocket.accept();
                System.out.println("客户端已连接成功");
                InputStream in= socket.getInputStream();
                InputStreamReader isr=new InputStreamReader(in, StandardCharsets.UTF_8);
                BufferedReader br=new BufferedReader(isr);
                String message;
                while ((message= br.readLine())!=null){
                    System.out.println("客户端说："+message);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server server=new Server();
        server.Start();
    }
}
