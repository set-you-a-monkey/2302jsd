package ooday05;
//static final常量演示
public class StaticFinalDemo {
    public static void main(String[] args) {
        //1）加载loo.class到方法区中
        //2）静态变量
        System.out.println(Loo.num);
        System.out.println(Loo.COUNT);
        System.out.println(Loo.PI);//常常通过类点来访问
        //Loo.PI=3.1415926;//编译错误，常量不能被改变

    }
}
class Loo{
    public static int num=5;
    public static final int COUNT=5;
    public static final double PI=3.14159;
    //public static final int NUM;//编译错误，常量必须声明同事初始化
}
