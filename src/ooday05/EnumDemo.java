package ooday05;

public class EnumDemo {
    public static void main(String[] args) {
        Season[] seasons=Season.values();//数组
        for(int i=0;i< seasons.length;i++){
            System.out.println(seasons[i].getSeasonDesc());//输出每个的描述属性
            System.out.println(seasons[i].getSeasonName());//输出每个枚举的名字属性
        }
        Season s=Season.AUTUMN;//
        System.out.println(s.getSeasonDesc()+","+s.getSeasonName());
        switch (s){
            case SPRING:
                System.out.println(s.getSeasonName()+"万物复苏");
            case SUMMER:
                System.out.println(s.getSeasonName()+"去游泳");
            case AUTUMN:
                System.out.println(s.getSeasonName()+","+"秋高气爽");
            case WINTER:
                System.out.println(s.getSeasonName()+","+"去滑雪");
        }

    }
}
