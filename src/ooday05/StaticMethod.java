package ooday05;
//静态方法的演示
public class StaticMethod {
    int a;
    static int b;
    void show(){
        System.out.println(a);
        System.out.println(b);
    }
    static void test(){
        //静态方法中没有隐式this传递
        //没有this传递意味着没有对象
        //而实例a必须通过对象来访问
        //所以如下语句发生编译错误
        //System.out.println(a);//编译错误，静态方法中不能直接访问实例成员
        System.out.println(b);
    }
    //在say()中需要访问对象的属性a，所以认为say的操作与对象有关，不适合设计为静态
    void say(){
        System.out.println(a);
    }
    //在plus()中不需要访问对象的属性/行为，所以认为plus的操作与对象无关，可以设计为静态
    static int plus(int num1,int num2){
        int num=num1+num2;
        return num;
    }
}
