package binary;
//16进制的演示
public class HexDemo {
    public static void main(String[] args) {
        int n=0x47b98a;
        int m=0b100_0111_1011_1001_1000_1010;
        System.out.println(Integer.toBinaryString(m));//二进制
        System.out.println(Integer.toBinaryString(n));
        System.out.println(Integer.toHexString(m));
        System.out.println(Integer.toHexString(n));//16进制
        System.out.println(m);
        System.out.println(n);//十进制
    }
}
