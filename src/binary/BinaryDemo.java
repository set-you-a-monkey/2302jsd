package binary;
//二进制的演示
public class BinaryDemo {
    public static void main(String[] args) {
        int n=45;
        System.out.println(Integer.toBinaryString(n));//二进制
        System.out.println(n);//十进制
        n++;
        System.out.println(Integer.toBinaryString(n));//++后二进制
        System.out.println(n);//十进制
    }
}
/*
   0100 0111 1011 1001 1000 1010
   4   7  b    9  8 a---47b98a
   0010 1001 1011 0110 0011 0101
   2     9    b     6   3    5--29b635
   1000 1111 0011 0101 1000 1001
   8    f     3   5     8    9--8f3589
   0001 1100 0111 0100 0110 0000 1111
   1   c      7    4    6     0   f--1c7460f
 */
