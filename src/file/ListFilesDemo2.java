package file;

import java.io.File;
import java.io.FileFilter;

public class ListFilesDemo2 {
    public static void main(String[] args) {
        File dir=new File(".");
        if (dir.isDirectory()) {//如果是一个目录
            FileFilter filter=new FileFilter() {
              //重写accept方法就是定义过滤器的接收条件
                //重写原则：当参数file对象是该过滤器接受的元素时返回true，否则返回false
                public boolean accept(File file) {
                    String name=file.getName();
                    return name.endsWith(".txt");//判断是否以“./tex”结尾
                }
            };
            File[] subs=dir.listFiles();
            for (File sub:subs){
                System.out.println(sub.getName());
            }

        }
    }
}
