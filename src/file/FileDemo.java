package file;

import java.io.File;

public class FileDemo {
    public static void main(String[] args) {
        File file=new File("./demo.txt");
        String name=file.getName();//获取file表示的文件或目录的名字
        System.out.println(name);
        Long len=file.length();//
        System.out.println(len);
        boolean cn=file.isHidden();
        boolean cr=file.canRead();
        boolean cw=file.canWrite();
        System.out.println("是否隐藏："+cn);
        System.out.println("是否只读："+cr);
        System.out.println("是否可写："+cw);
    }
}
