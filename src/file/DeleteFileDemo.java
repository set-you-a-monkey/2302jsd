package file;

import java.io.File;

//删除文件或目录
public class DeleteFileDemo {
    public static void main(String[] args) {
       // File f=new File("./test.tst");
      //  File f=new File("./demo");
        File f=new File("./a/b/c");//删除a下的b下的c
       // File f=new File("./a")//a目录中有内容，所以不能直接删除

        if (f.exists()){
            f.delete();
            System.out.println("该文件或目录已删除");
        }else {
            System.out.println("该文件或目录不存在");
        }
    }
}
