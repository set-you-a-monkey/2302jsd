package file;

import java.io.File;
import java.nio.file.Files;

//访问一个目录中的所有子项
public class ListFilesDemo {
    public static void main(String[] args) {
        File dir=new File("./m/y/u");
        if (dir.isDirectory()){
          File[] subs=dir.listFiles();
          System.out.println("当前目录下共有"+ subs.length+"个子项");

        }
        if (dir.isFile()){
            System.out.println("表示文件已存在");
        }else {
            System.out.println("文件不存在");
        }
    }
}
