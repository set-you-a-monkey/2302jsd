package file;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Text05 {
    public static void main(String[] args) throws IOException {
        Scanner scan=new Scanner(System.in);
        while (true){
            System.out.println("请输入文件名");
            String name=scan.nextLine();
            File file=new File(name);
            if (file.exists()){
                System.out.println("该文件已存在，请重新输入");
            }else {
                file.createNewFile();
                System.out.println("该文件创建成功");
                break;
            }
        }







        /*
        Scanner scan=new Scanner(System.in);
        while (true){
            System.out.println("请输入文件名字：");
            String name=scan.nextLine();
            File file=new File(name);
            if (file.exists()){
                System.out.println("该文件已经存在，请重新输入");
            }else {
                file.createNewFile();
                System.out.println("该文件创建成功");
                break;
            }
        }

         */

    }
}
