package exception;

public class ThrowsDemo {
    public static void main(String[] args) {
        Student zs=new Student();
        try {
            zs.setAge(1000);
        } catch (IllegalAgeException e) {
            e.printStackTrace();//打印错误堆栈信息
            //System.out.println(e.getMessage());//获取错误信息并输入
        }
        System.out.println("此人的年龄为"+zs.getAge()+"岁");
    }
}
