package exception;

import java.io.FileNotFoundException;

//异常抛出
public class ThrowDemo {
    public static void main(String[] args) {


        Student zs=new Student();
        try {
            zs.setAge(1000);
        } catch (IllegalAgeException e) {
            System.out.println("发生异常，并处理了");
        }
        System.out.println("此人年龄为"+zs.getAge()+"岁");






        /*
        Student zs=new Student();
        try{
            zs.setAge(1000);
            System.out.println("此人的年龄为"+zs.getAge()+"岁");
        }catch (RuntimeException e){
            System.out.println("年龄异常");
        }

         */



    }
}
