package exception;

import java.io.FileNotFoundException;

public class Student {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws IllegalAgeException {//抛给调用者
        if (age<0||age>100){
            throw new IllegalAgeException("年龄不合法");//抛出检查异常，是必须被处理的
        }
        this.age = age;
    }
}
