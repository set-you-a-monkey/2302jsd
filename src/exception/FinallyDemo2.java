package exception;

import java.io.FileOutputStream;
import java.io.IOException;

public class FinallyDemo2 {
    public static void main(String[] args) {
        FileOutputStream fos=null;
        System.out.println("程序开始了");
        try{
            fos=new FileOutputStream("./a/b/fos.dat");
        }catch (IOException e){
            System.out.println("出现io异常并解决了");
        }finally {
            try {
                fos.close();
            } catch (Exception e) {
               System.out.println("关闭流出现异常并解决了");
            }
        }
        System.out.println("程序结束了");
    }
}
