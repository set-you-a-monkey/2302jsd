package exception;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;

public class ThrowsDemo2 {
    public void dosome () throws IOException, AWTException{

    }
}
class SubClass extends ThrowsDemo2{
    @Override
   // public void dosome() throws IOException, AWTException { }
   // public void dosome() { }
    /*
    public void dosome() throws IOException{

    }

     */
    public void dosome() throws FileNotFoundException {

    }
}
