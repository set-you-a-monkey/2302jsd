package exception;

public class TryCatchDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        //try
        try {
            String line=null;
            System.out.println(line.length());
            String line1="";//应为上一句发生异常，所以try块中此句及以下代码都不会执行了
             System.out.println(line1.charAt(0));
            String line2="abc";
            System.out.println(Integer.parseInt(line2));
        }catch (NullPointerException e){
            System.out.println("出现空指针异常并解决了");
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("出现了下标越界的异常并解决了");
        }catch (Exception e){
            System.out.println("反正错了并解决了");
        }

        System.out.println("程序结束了");
    }
}
