package io;

import java.io.*;

//缓冲流：是一对高级流，作用是加快读写效率
public class CopyDemoBuffered {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("ccc.jpg");
        BufferedInputStream bis=new BufferedInputStream(fis);
        FileOutputStream fos=new FileOutputStream("ccc_py3.jpg");
        BufferedOutputStream bos=new BufferedOutputStream(fos);
        long start=System.currentTimeMillis();
        int d;
        while ((d=bis.read())!=-1){
            bos.write(d);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成，耗时："+(end-start)+"毫秒");
        bis.close();
        bos.close();

    }
}
