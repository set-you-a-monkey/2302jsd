package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class TestNote {
    public static void main(String[] args) throws IOException {
        Scanner scan=new Scanner(System.in);
        FileOutputStream fos=new FileOutputStream("note.txt");
        while (true){
            System.out.println("请输入内容,输入exit自动退出");
            String line= scan.nextLine();

            if ("exit".equals(line)) {
                break;

            }
            byte[] data=line.getBytes(StandardCharsets.UTF_8);
            fos.write(data);
        }
        System.out.println("写入完成");
        fos.close();
    }
}
