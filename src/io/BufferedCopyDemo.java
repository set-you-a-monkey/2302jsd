package io;

import java.io.*;

public class BufferedCopyDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("ccc.jpg");
        BufferedInputStream bis=new BufferedInputStream(fis);
        FileOutputStream fos=new FileOutputStream("ccc_cp5.jpg");
        BufferedOutputStream bos=new BufferedOutputStream(fos);
        long start=System.currentTimeMillis();
        int d;
        while ((d=bis.read())!=-1){
            bos.write(d);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成！耗时："+(end-start)+"毫秒");
        bis.close();
        bos.close();







    /*
        FileInputStream fis=new FileInputStream("ccc.jpg");
        BufferedInputStream bis=new BufferedInputStream(fis);
        FileOutputStream fos=new FileOutputStream("ccc_cp4.jpg");
        BufferedOutputStream bos=new BufferedOutputStream(fos);
        long start=System.currentTimeMillis();
        int d;
        while ((d= bis.read())!=-1){
            bos.write(d);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成!耗时："+(end-start)+"毫秒");
        bis.close();
        bos.close();

     */
    }
}
