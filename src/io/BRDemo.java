package io;

import java.io.*;
import java.nio.charset.StandardCharsets;

//缓冲字符流输入流读取文本数据
public class BRDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("pw.txt");
        InputStreamReader isr=new InputStreamReader(fis, StandardCharsets.UTF_8);
        BufferedReader br=new BufferedReader(isr);

        String line;
        while ((line=br.readLine())!=null){
            System.out.println(line);
        }
        br.close();
    }
}
