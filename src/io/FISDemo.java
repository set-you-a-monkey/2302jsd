package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

//文件输入流，读取文件
public class FISDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("fos.dat");//定义文件输入流
        int d=fis.read();//读取第一个字节
        System.out.println(d);//输出10进制数据：1
        //d=fis.read();//读取第二个字节
        //System.out.println(d);//输出10进制数据：2
        //d= fis.read();//
        //System.out.println(d);
        fis.close();//关闭流
    }
}
