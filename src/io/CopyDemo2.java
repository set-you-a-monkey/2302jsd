package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo2 {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("6fd910b9e7615e14bafa326e5d662e28_1.jpg");
        FileOutputStream fos=new FileOutputStream("6fd910b9e7615e14bafa326e5d662e28_1_cp2.jpg");
        long start=System.currentTimeMillis();
        byte[] date=new byte[1024*10];
        int len;//记录每次实际读取的字节数
        while ((len=fis.read(date))!=-1){//每次读取10kb并判断对否读取到了末尾
            fos.write(date,0,len);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成！消耗"+(end-start)+"毫秒");
        fis.close();
        fos.close();








        /*
        long start=System.currentTimeMillis();
        byte[] date=new byte[1024*10];
        while ((fis.read(date))!=-1){
            fos.write(date);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成！消耗"+(end-start)+"毫秒");
        fis.close();
        fos.close();



         */
    }
}
