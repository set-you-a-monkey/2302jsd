package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class WriteStringDemo1 {
    public static void main(String[] args) throws IOException {
        FileOutputStream fis=new FileOutputStream("fis.txt");
        String line="心在跳是爱情如烈火，你在笑疯狂的是我";
        byte[] data=line.getBytes(StandardCharsets.UTF_8);
        fis.write(data);
        fis.write("爱如火温暖了心窝".getBytes(StandardCharsets.UTF_8));
        System.out.println("写入完毕");
        fis.close();




        /*
        FileOutputStream fis=new FileOutputStream("fis.txt",true);
        String line="心在跳是爱情如烈火，你在笑疯狂的是我";
        byte[] data=line.getBytes(StandardCharsets.UTF_8);
        fis.write(data);
        fis.write(",爱如火温暖了心窝".getBytes(StandardCharsets.UTF_8));
        System.out.println("写出完毕");
        fis.close();

         */
    }
}
