package io;

import java.io.*;

//对象流：处理流高级流
public class OOSDemo {
    public static void main(String[] args) throws IOException {
        String name="张三";
        int age=25;
        String gender="男";
        String[] otherInfo={"是一名学生，","来自黑龙江佳木斯，爱好女，喜欢打篮球"};
        Student zs=new Student(name,age,gender,otherInfo);
        System.out.println(zs);
        FileOutputStream fos=new FileOutputStream("student.obj");
        ObjectOutput oos=new ObjectOutputStream(fos);
        oos.writeObject(zs);
        System.out.println("写出完毕");
        oos.close();
    }
}
