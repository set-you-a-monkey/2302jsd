package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FOSDemo {
    public static void main(String[] args) throws IOException {
        File file=new File("fos.dat");
        FileOutputStream fos=new FileOutputStream(file);
        //FileOutputStream fos=new FileOutputStream("fos.dat");//定义文件输出流
        //fos.write(1);//向fos。dat中写入1个字节
        //fos.write(2);
        fos.write(5000);
        System.out.println("写出完毕");
        fos.close();//关闭流
    }
}
