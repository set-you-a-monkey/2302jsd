package io;

import java.io.*;
import java.nio.charset.StandardCharsets;

//缓冲字符流
public class PWDemo {
    public static void main(String[] args) throws IOException {
        PrintWriter pw=new PrintWriter("pw.txt");
        pw.println("和我在成都的街头走一走，哦哦哦。。");
        pw.println("直到所有的灯都熄灭了也不停留");
        System.out.println("写出完毕！");
        pw.close();




       /*
        FileOutputStream fos=new FileOutputStream("pw.txt");
        OutputStreamWriter osw=new OutputStreamWriter(fos, StandardCharsets.UTF_8);
        BufferedWriter bw=new BufferedWriter(osw);
        PrintWriter pw=new PrintWriter(bw);
        pw.println("和我在成都的街头走一走，哦哦哦...");
        pw.println("直到所有的灯都熄灭了也不停留");
        System.out.println("写出完毕！");
        bw.close();

        */


    }
}
