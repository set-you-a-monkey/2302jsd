package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

//转换流
public class OSWDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fis=new FileOutputStream("osw.txt");
        OutputStreamWriter osw=new OutputStreamWriter(fis, StandardCharsets.UTF_8);
        osw.write("和我在成都的街头走一走，哦哦哦...");
        osw.write("知道所有的灯都熄灭了也不停留");
        System.out.println("写出完成");
        osw.close();
    }
}
