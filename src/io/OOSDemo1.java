package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class OOSDemo1 {
    public static void main(String[] args) throws IOException {
        String name="张三";
        int age=25;
        String gender="男";
        String[] otherInfo={"来自黑龙江佳木斯，爱好女，喜欢打篮球"};
        Student1 zs=new Student1(name,age,gender,otherInfo);
        System.out.println(zs);
        FileOutputStream fos=new FileOutputStream("student1.txt");
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(zs);
        System.out.println("写出完毕");
        oos.close();













        /*
        String name="张三";
        int age=25;
        String gender="男";
        String[] otherInfo={"来自黑龙江佳木斯，爱好女，喜欢篮球"};
        Student1 zs=new Student1(name,age,gender,otherInfo);
        System.out.println(zs);
        FileOutputStream fos=new FileOutputStream("student1.txt");
        ObjectOutputStream oos=new ObjectOutputStream(fos);
        oos.writeObject(zs);
        System.out.println("写出完毕");
        oos.close();

         */
    }
}
