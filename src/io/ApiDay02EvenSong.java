package io;

public class ApiDay02EvenSong {
    public static void main(String[] args) {
        Aoo o1=new Aoo();
        o1.a=5;
        Aoo o2=o1;//将o1的地址做了个副本后赋值给o2了，是指同一个对象
        o2.a=8;
        System.out.println(o1.a);
        int a=5;
        int b=a;
        System.out.println(b);
        b=8;
        System.out.println(a);

    }
}
class Aoo{
    int a;
}