package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//文件复制：将一个文件的数据读出来 ，然后写入到灵一个文件中
public class CopyDemo {
    public static void main(String[] args) throws IOException {
        FileInputStream fis=new FileInputStream("6fd910b9e7615e14bafa326e5d662e28_1.jpg");
        FileOutputStream fos=new FileOutputStream("6fd910b9e7615e14bafa326e5d662e28_1_cp.jpg");
        long start=System.currentTimeMillis();//自1970年1月1日时到此时此刻的毫秒数
        int d;//记录每次读取到的字节
        while ((d= fis.read())!=-1){//每次循环读取1个字节到d中，判断若不是负1，表示没有读取末尾
            fos.write(d);//将读出来的字节写入到要复制的文件中
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成!耗时"+(end-start)+"ms");
        fis.close();
        fos.close();

    }
}
