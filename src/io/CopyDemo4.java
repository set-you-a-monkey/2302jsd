package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo4 {
    public static void main(String[] args) throws IOException {
        FileInputStream fos=new FileInputStream("ccc.jpg");
        FileOutputStream fis=new FileOutputStream("ccc_cp2.jpg");
        long start=System.currentTimeMillis();
        byte[] data=new byte[1024*10];
        int len;
        while ((len=fos.read(data))!=-1){
            fis.write(data,0,len);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成！耗时："+(end-start)+"毫秒");
        fos.close();
        fis.close();






        /*
        FileInputStream fos=new FileInputStream("ccc.jpg");
        FileOutputStream fis=new FileOutputStream("ccc_cp2.jpg");
        long start=System.currentTimeMillis();
        byte[] data=new byte[1024*10];
        int len;
        while ((len=fos.read(data))!=-1){
            fis.write(data,0,len);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制成功!耗时："+(end-start)+"毫秒");
        fos.close();
        fis.close();

         */
    }
}
