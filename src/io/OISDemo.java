package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class OISDemo {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileInputStream fis=new FileInputStream("student.obj");
        ObjectInputStream ois=new ObjectInputStream(fis);
        Object obj=ois.readObject();
        Student zs=(Student) obj;
        System.out.println(zs);
        ois.close();
    }
}
