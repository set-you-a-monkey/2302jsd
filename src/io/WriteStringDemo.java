package io;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

//向文件中写入文本数据
public class WriteStringDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fos=new FileOutputStream("fos.txt",true);
        String line="心在跳是爱情如烈火，你在笑疯狂的人是我。";
        //将line的内容按照utf-8编码转换为字节数组
        byte[] data=line.getBytes(StandardCharsets.UTF_8);
        fos.write(data);
        fos.write("爱如火会温暖了心窝".getBytes(StandardCharsets.UTF_8));
        System.out.println("写入完毕");
        fos.close();
    }
}
