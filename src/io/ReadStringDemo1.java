package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class ReadStringDemo1 {
    public static void main(String[] args) throws IOException {
        File file=new File("fis.txt");
        FileInputStream fos=new FileInputStream(file);
        byte[] data=new byte[(int)file.length()];
        fos.read(data);
        String line=new String(data,StandardCharsets.UTF_8);
        System.out.println(line);
        fos.close();





        /*
        File file=new File("fis.txt");
        FileInputStream fos=new FileInputStream(file);
        byte[] data=new byte[(int)file.length()];
        fos.read(data);
        String line=new String(data, StandardCharsets.UTF_8);
        System.out.println(line);
        fos.close();

         */
    }
}
