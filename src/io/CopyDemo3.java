package io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyDemo3 {
    public static void main(String[] args) throws IOException {
        FileInputStream fos=new FileInputStream("ccc.jpg");
        FileOutputStream fis=new FileOutputStream("ccc_cp.jpg");
        long start=System.currentTimeMillis();
        int d;
        while ((d=fos.read())!=-1){
            fis.write(d);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完成！耗时："+(end-start)+"毫秒");
        fos.close();
        fis.close();



        /*
        FileInputStream fos=new FileInputStream("ccc.jpg");
        FileOutputStream fis=new FileOutputStream("ccc_cp.jpg");
        long start=System.currentTimeMillis();
        int d;
        while ((d=fos.read())!=-1){
            fis.write(d);
        }
        long end=System.currentTimeMillis();
        System.out.println("复制完毕！耗时："+(end-start)+"毫秒");
        fos.close();
        fis.close();

         */
    }
}
