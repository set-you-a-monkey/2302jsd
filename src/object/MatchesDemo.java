package object;

import java.util.Arrays;

public class MatchesDemo {
    public static void main(String[] args) {
        String line="abc123def456ghi78";
        String[] data=line.split("[0-9]+");
        System.out.println(Arrays.toString(data));
        line="123.456.78";
        data=line.split("\\.");
        System.out.println(Arrays.toString(data));
        line=".123.456..789....";
        data=line.split("\\.");
        System.out.println(Arrays.toString(data));
        System.out.println(data.length);

    }
}
/*
        //String[] Split(String regex):拆分
        String line="abc123def456ghi78";
        String[] data=line.split("[0-9]+");
        System.out.println(Arrays.toString(data));
        line="123.456.78";
        data=line.split("\\.");
        System.out.println(Arrays.toString(data));
        line=".123.456..78....";
        data=line.split("\\.");
        System.out.println(Arrays.toString(data));
        System.out.println(data.length);

         */
 /*
       String line="abc123def456ghi78";
       //将数字部分替换为#NUMBER#
        line=line.replaceAll("[0-9]+","#NUMBER");
        System.out.println(line);

         */
 /*
        String line="abc123def456ghi78";
        //将数字部分替换为#NUMBER#
        line=line.replaceAll("[0-9]+","#NUMBER#");
        System.out.println(line);

         */
 /*
        //邮箱正确表达式：
        //[a-zA-Z0-9]+@[a-zA-Z0-9]+[\*[a-zA-Z]+)+
        String email="wangkj@tedu.cn";
        String regex="[a-zA-Z0-9]+@[a-zA-Z0-9]+[\\*[a-zA-Z]+)+";
        boolean match=email.matches(regex);
        if (match){
            System.out.println("是正确的邮箱格式");
        }else{
            System.out.println("不是正确的邮箱格式");
        }

         */
/*
        //邮箱正确表达式：
        //[a-zA-Z0-9]+@[a-zA-Z0-9]+(\.[a-zA-Z]+)+
        String email="wangkj@tedu.cn";
        String regex="[a-zA-Z0-9]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";
        boolean match=email.matches(regex);
        if (match){
            System.out.println("正确的邮箱格式");
        }else{
            System.out.println("不正确的邮箱格式");
        }

         */