package object;

public class IntegerDemo1 {
    public static void main(String[] args) {
        //演示包装的定义
        Integer i1=new Integer(5);
        Integer i2=new Integer(5);
        System.out.println(i1==i2);//==比较的是地址
        System.out.println(i1.equals(i2));//true,包装类重写equals（）比较值了
        //valueof()方法会复用1个字节（-128到127）范围内的数据，建议使用valueof（）——
        Integer i3=Integer.valueOf(5);
        Integer i4=Integer.valueOf(5);
        System.out.println(i3==i4);
        System.out.println(i3.equals(i4));
        //将包装类转换为基本类型
        int i=i4.byteValue();
        System.out.println(i);
        //演示包装类的常用操作：
        //1)可以通过包装类来获取基本类型的取值范围
        int max=Integer.MAX_VALUE;
        int min=Integer.MIN_VALUE;
        System.out.println("int的最大值为"+max+",最小值为"+min);
        long max1=Long.MAX_VALUE;
        long min1=Long.MIN_VALUE;
        System.out.println("long的最大值为"+max1+"最小值为"+min1);
        String s1="39";
        int age=Integer.parseInt(s1);
        System.out.println(age);
        String s2="123.456";
        double prince=Double.parseDouble(s2);
        System.out.println(prince);

    }
}
/*
    //演示包装的定义
    Integer i1=new Integer(5);
    Integer i2=new Integer(5);
        System.out.println(i1==i2);//==比较的是地址
                System.out.println(i1.equals(i2));//true,包装类重写equals（）比较值了
                //valueof()方法会复用1个字节（-128到127）范围内的数据，建议使用valueof（）——
                Integer i3=Integer.valueOf(5);
                Integer i4=Integer.valueOf(5);
                System.out.println(i3==i4);
                System.out.println(i3.equals(i4));
                //将包装类转换为基本类型
                int i=i4.byteValue();
                System.out.println(i);
                //演示包装类的常用操作：
                //1)可以通过包装类来获取基本类型的取值范围
                int max=Integer.MAX_VALUE;
                int min=Integer.MIN_VALUE;
                System.out.println("int的最大值为"+max+",最小值为"+min);
                long max1=Long.MAX_VALUE;
                long min1=Long.MIN_VALUE;
                System.out.println("long的最大值为"+max1+"最小值为"+min1);
                String s1="39";
                int age=Integer.parseInt(s1);
                System.out.println(age);
                String s2="123.456";
                double prince=Double.parseDouble(s2);
                System.out.println(prince);

 */