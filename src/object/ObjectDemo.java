package object;

public class ObjectDemo {
    public static void main(String[] args) {
        String s1=new String("hello");
        String s2=new String("hello");

        System.out.println(s1.equals(s2));
        StringBuilder builder1=new StringBuilder("hello");
        StringBuilder builder=new StringBuilder("hello");
        System.out.println(builder1.equals(builder));
        //s1与builder1类型不同，所以equals是false
        System.out.println(s1.equals(builder1));//false











/*

        Point P1=new Point(100,200);
        Point p2=new Point(100,200);
        System.out.println(P1==p2);//比较的是地址false
        System.out.println(P1.equals(p2));

         */
        /*
        Point p=new Point(100,200);
        System.out.println(p);
        System.out.println(p.toString());

         */
    }
}
