package object;

public class IntegerDemo {
    public static void main(String[] args) {
        //可以通过包装类来获取进本类型的取值范围
        int max=Integer.MAX_VALUE;
        int min=Integer.MIN_VALUE;
        System.out.println("最大值为"+max+"，最小值为"+min);
        long max1=Long.MAX_VALUE;
        long min1=Long.MIN_VALUE;
        System.out.println("最大值为"+max1+",最小值为"+min1);
        //2)包装类可以通过将字符串转换为对应的基本类型--要熟练
        String s1="39";
        int age=Integer.parseInt(s1);//将字符串s1转换为int类型并赋值给age
        System.out.println(age);
        String s2="123.456";
        double price=Double.parseDouble(s2);//将字符串类型s2转换为double类型并赋值给price
        System.out.println(price);













                Integer i1=new Integer(5);
        Integer i2=new Integer(5);
        System.out.println(i1==i2);//false//比较的是地址
        System.out.println(i1.equals(i2));//true//包装类重写equals
        //valueof()方法复用1个字节（-128到127）范围内的数据，建议使用valueof（）
        Integer i3=Integer.valueOf(5);
        Integer i4=Integer.valueOf(5);
        System.out.println(i3==i4);//
        System.out.println(i3.equals(i4));//true//包装类equals重写
        //触发了自定装箱特性，会被编译为：integer i=Integer.valueof(5);
        Integer i=5;//基本类型到包装类---装箱

        int j=i;//包装类型大奥基本类型----拆箱
    }
}
