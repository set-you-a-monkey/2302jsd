package work;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FinallyDemo2 {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        FileOutputStream fos=null;
        try{
            fos=new FileOutputStream("./a/b/c/fos.txt");
            fos.write(1);
        }catch (IOException e){
            System.out.println("出现oi异常，并已解决");
        }finally {
            try {
                if (fos!=null){
                    fos.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("程序已结束");




        /*
        System.out.println("程序开始了");
        FileOutputStream fos=null;
        try {
            fos=new FileOutputStream("./a/b/c/fos.txt");
            fos.write(1);
        }catch (IOException e){
            System.out.println("出现oi异常，并已解决");
        }finally {
            try {
                if (fos!=null){
                    fos.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("程序结束了");

         */
    }
}
