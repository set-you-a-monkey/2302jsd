package work;

public class FinallyDemo {
    public static void main(String[] args) {
        System.out.println("程序开始了");
        try{
            String line="";
            System.out.println(line.charAt(0));
            return;
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("出现下标异常，并已解决" );
        }finally {
            System.out.println("finally中的代码继续执行");
        }
        System.out.println("程序结束了");


        /*

        System.out.println("程序开始了");
        try {
            String line="";
            System.out.println(line.charAt(0));
            return;
        }catch (StringIndexOutOfBoundsException e){
            System.out.println("出现下标异常，并已解决");
        }finally {
            System.out.println("代码继续执行");
        }
        System.out.println("程序结束了");

         */

    }
}
