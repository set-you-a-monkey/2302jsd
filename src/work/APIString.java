package work;

import java.util.Locale;

public class APIString {
    public static void main(String[] args) {
        String str = new String("PersoN");
        StringBuilder str1 = new StringBuilder("peOple");
        //StringBuilder转为String
        String str2 = new String(str1);
        String str3 = new String(str + str2);

        StringBuilder str4 = new StringBuilder(str3);
        for (int i = 0; i < str4.length(); i++) {
            if (Character.isLowerCase(str4.charAt(i))) {
                System.out.println(str4.charAt(i)+"");
            } else {
                System.out.println((str4.charAt(i) + "").toLowerCase());
            }

        }
    }
}
