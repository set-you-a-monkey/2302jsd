package work;

import java.util.ArrayList;
import java.util.Scanner;

public class List {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        ArrayList<Integer> list=new ArrayList<>();
        int sum=0;
        while (sum<=200){
            System.out.println("请输入1-100之间的整数数");
            int num=Integer.parseInt(scan.nextLine());
            if (num<0||num>100){
                System.out.println("输入的数不在1-100之内，请重新输入");
                break;
            }
            list.add(num);
            sum=getSum(list);
        }
        System.out.println(sum);


    }
    private static int getSum(ArrayList<Integer>list) {
        int sum = 0;
        for (Integer num : list) {
            sum += num;
        }
        return sum;
    }

}
