package work;

public class Student {

    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age<0||age>100){
            throw new RuntimeException();
        }
        this.age = age;
    }
    /*
    private int age;



    public int getAge() {
        return age;
    }

    public void setAge(int age)  {
        if (age<0||age>100){
            throw new RuntimeException();
        }
        this.age = age;
    }

     */


}
