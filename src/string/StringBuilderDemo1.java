package string;

public class StringBuilderDemo1 {
    public static void main(String[] args) {

        String str="好好学习java";
        StringBuilder builder=new StringBuilder(str);
        builder.append(",为了找个好工作");
        System.out.println(builder);
        builder.replace(9,16,"就是为了改变世界");
        System.out.println(builder);
        builder.delete(0,8);
        System.out.println(builder);
        builder.insert(0,"活着");
        System.out.println(builder);


    }
}
/*
        String str="好好学习java";
        StringBuilder builder=new StringBuilder(str);
        builder.append(",为了找个好工作");
        System.out.println(builder);
        builder.replace(9,16,"就是为了改变世界");
        System.out.println(builder);
        builder.delete(0,8);
        System.out.println(builder);
        builder.insert(0,"活着");
        System.out.println(builder);

         */