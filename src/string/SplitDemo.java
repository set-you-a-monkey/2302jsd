package string;

import java.util.Arrays;

public class SplitDemo {
    public static void main(String[] args) {
        String line="abc123def456ghi";
        String[] data=line.split("[0-9]+");//按数字拆分
        System.out.println(Arrays.toString(data));
        line="123.456.78";
        data=line.split("\\.") ;
        line=".123.456..78......";
        data=line.split("\\.");//按.拆（.就拆没了）
        System.out.println(Arrays.toString(data));
        System.out.println(data.length);

    }
}
