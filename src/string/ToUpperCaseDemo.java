package string;

//String toUpperCase//将当前字符串的英文部分转换为大写
//String toLowerCase//将当前字符串的英文部分转换为小写
public class ToUpperCaseDemo {
    public static void main(String[] args) {
        String str="我爱Java!";
        String upper=str.toUpperCase();//将str中的英文部分准换为大写，并赋值给upper
        System.out.println(upper);
        String lower=str.toLowerCase();
        System.out.println(lower);
    }
}
