package string;
//
public class IndexOfDemo {
    public static void main(String[] args) {
        String str="thinking in java";
       int index=str.indexOf("in");//检索in在str中第一次出现的位置
       System.out.println(index);//2
       index=str.indexOf("in",3);//从下标为3的位置开始找in第一次出现
       System.out.println(index);//5
       index=str.indexOf("abc");//若字符串在str中不存在，则返回是-1
        System.out.println(index);
        index=str.lastIndexOf("in");//检索in在str中最后一次出现的位置
        System.out.println(index);//9

    }
}
