package string;

public class StringBuilderDemo {
    public static void main(String[] args) {
        String str="好好学习java";
        //复制str的内容到builder中---好好学习java
        StringBuilder builder=new StringBuilder(str);
        //append():追加内容---在末尾加东西
        builder.append(",为了找个好工作");
        System.out.println(builder);
        //replace():替换部分内容（含头不含尾）；
        builder.replace(9,16,"就是为了改变世界");
        System.out.println(builder);
        //delete():删除部分内容（焊头不含尾）
        builder.delete(0,8);
        System.out.println(builder);
        //insert():插入内容
        builder.insert(0,"活着");
        System.out.println(builder);









        /*
        //StringBuilder的创建方式：
        StringBuilder builder=new StringBuilder();//空字符串
        StringBuilder builder2=new StringBuilder("abc");
        String str="abc";
        StringBuilder builder3=new StringBuilder(str);//abc串
        String str2=builder3.toString();

         */
    }
}
