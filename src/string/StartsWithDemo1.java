package string;

public class StartsWithDemo1 {
    public static void main(String[] args) {
       String str="thinking in java";
       boolean starts=str.startsWith("think");
       System.out.println(starts);
       boolean ends=str.endsWith(".enp");
       System.out.println(ends);
    }
}
 /*
        String str="thinking in java";
        boolean starts=str.startsWith("think");
        System.out.println(starts);
        boolean ends=str.endsWith(".enp");
        System.out.println(ends);

         */