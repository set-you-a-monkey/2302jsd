package string;
//Trim去除字符串两边的空白，中间的不
public class TrimDemo1 {
    public static void main(String[] args) {
       String str="    hello world   ";
       str=str.trim();
       System.out.println(str);//去除str两边的空白符，（一个新的字符串对象）并存到str
    }
}
 /*
        String str="   hello world   ";
        str=str.trim();
        System.out.println(str);

         */