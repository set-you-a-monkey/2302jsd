package string;

public class MatchesDemo {
    public static void main(String[] args) {
        //邮箱正则表达式 [a-zA-Z0-9_]+@[a-zA-Z0-9]+(\.[a-zA-Z]+)+
        String email = "wangkj@tesu.cn";
        String regex = "[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";
        boolean math = email.matches(regex);
        if (math) {
            System.out.println("正确的邮箱格式");
        } else {
            System.out.println("错误邮箱格式");
        }
    }
}
