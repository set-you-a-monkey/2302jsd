package string;

public class IndexOfDemo1 {
    public static void main(String[] args) {
        String str="thinking in java";
        int index=str.lastIndexOf("in");//检索in在str中第一次出现的位置
        System.out.println(index);
        index=str.lastIndexOf("in");//检索in在str中最后出现的位置
        System.out.println(index);
    }
}
/*
        String str="thinking in java";
        int index=str.indexOf("in");
        System.out.println(index);
        index=str.lastIndexOf("in");
        System.out.println(index);

         */