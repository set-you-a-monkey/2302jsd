package string;
//string的演示
public class StringDemo {
    public static void main(String[] args) {
        String s=new String("hello");//创建了两个对象
        //第一个字面量
        //第二个new String()
        String s1="hello";
        System.out.println(s==s1);//false,==比较的是地址是否相同
        System.out.println(s.equals(s1));//true,equals()比较的是内容是否相同
        //在实际应用中，String 比较相等一般都只比较字符串的内容是否相同
        //因此需要用equals（）方法来比较两个字符串内容是否相同

        /*
        String s1="123abc";//在堆中创建123abc字符量对象，并缓存到常量池中
        //编译器在编译时，若是发现是两个字面量相连，则会直接连接好并将结果保存起来
        //如下语句会被编译为：String s2="123abc";
        String s2="123"+"abc";
        System.out.println(s1==s2);//true
        String s3="123";
        //以为s3是一个变量所以在编译期不会被直接编译
        String s4=s3+"abc";
        System.out.println(s1==s4);//false

         */
        /*
        String s1="123abc";
        String s2="123abc";
        String s3="123abc";
        System.out.println(s1==s2);
        System.out.println(s1==s3);
        System.out.println(s2==s3);
        s1=s1+"!";
        System.out.println(s1==s2);

         */
    }
}
