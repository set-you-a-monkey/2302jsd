package string;
//String substring(int stat,int end)
//截取当前字符串指定范围的字符串（含头不含尾--包含start，但不包含end）
public class SubstringDemo1 {
    public static void main(String[] args) {
       String str="www.tedu.cn";
       int start=str.indexOf(".")+1;
       int end=str.indexOf(".",start);
       String name=str.substring(start,end);
       System.out.println(name);
    }
}
 /*
        String str="www.tedu.cn";
        int start=str.indexOf(".")+1;
        int end=str.indexOf(".",start);
        String name=str.substring(start,end);
        System.out.println(name);

         */