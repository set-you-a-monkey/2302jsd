package string;
//boolean startsWith(String s)判断当前字符串是否是以给定的字符串（s)开始的
public class StartsWithDemo {
    public static void main(String[] args) {
        String str="thinking in java";
        boolean starts=str.startsWith("think");
        System.out.println(starts);

        boolean ends=str.endsWith(".enp");
        System.out.println(ends);

    }
}
